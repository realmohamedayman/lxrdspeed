--[[



____  ___________________             
|  |  |_________________|
|  |          |  | 
|  |          |  |
|  |          |  |
|  |          |  |
|  |          |  |
|  |          |  |
|  |____      |  |
|_______|     |__|



Small Speed Hack -- By Lxrd
Press 'E' to activate speed

]]--











-- Variables
local WalkSpeed = Instance.new("ScreenGui")
local mainframe = Instance.new("Frame")
local button = Instance.new("TextButton")


--MainGUI
WalkSpeed.Name = "WalkSpeed"
WalkSpeed.Parent = game.CoreGui



--MainFrame
mainframe.Name = "MainFrame"
mainframe.Parent= WalkSpeed
mainframe.AnchorPoint = Vector2.new(0.5 , 0.5)
mainframe.BackgroundColor3 = Color3.fromRGB(255 , 255 , 255)
mainframe.BackgroundTransparency = 0
mainframe.Position = UDim2.new(0.5, 0, 0.5, 0)
mainframe.BorderSizePixel = 0


--Button
button.Name = "Button1"
button.Parent = mainframe
button.AnchorPoint = Vector2.new(0.5, 0.5)
button.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
button.BorderSizePixel = 1
button.Position = UDim2.new(1, 0, 0.5, 0)
button.Size = UDim2.new(1, 0, 1, 0)
button.Text = 'Press E to activate speed'








--Script
local userInputService = game:GetService("UserInputService")
userInputService.InputBegan:Connect(function(input , gameProcessedEvent)
    if input.KeyCode == Enum.KeyCode.E then	
	
    local speed = 70

	game:GetService("Players").LocalPlayer.Character.Humanoid.WalkSpeed = speed
    game:GetService("Players").LocalPlayer.Character.Humanoid.JumpPower = (speed * 1.5)
		
	--remove on screen word
	button:Destroy()	
		
	end
end)

